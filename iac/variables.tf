variable "cidr_block" {
  description = "CIDR block for subnets"
  default = "10.10.0.0/18"
}