
output "vpc_id" {
  value = aws_vpc.default.id
}

output "public_subnets_id" {
  value = aws_subnet.public.*.id
}

output "private_subnets_id" {
  value = aws_subnet.private.*.id
}

output "public_sg" {
  value = aws_network_acl.public.id
}

output "private_sg" {
  value = aws_network_acl.private.id
}
