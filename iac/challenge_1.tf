
terraform {
  required_version = ">= 0.13.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
### Didn't understood cidrsubnet concept so skipped this part.


### Allowing to work on same TF state with lock. 
#### Assuming here that s3 bucket is created/ dynamoDB in place ( didn't have time to add)
terraform {
  backend "s3" {
    bucket         = "humn-global-tf-state"
    key            = "terraform-humn-global.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "humn-global-tf-state"
    profile        = "kkdigital"
  }

}




provider "aws" {
  region = "eu-central-1"

}


data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_caller_identity" "current" {}


locals {
  subnet_count = length(data.aws_availability_zones.available.names)
  region       = "eu-west-1"
}

resource "aws_vpc" "default" {
  cidr_block = "10.10.0.0/18"

  tags = {
    Name = "humn-vpc"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.default.id
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id

  tags = {
    Name = "internet-gw"
  }
}


resource "aws_eip" "default" {
  vpc        = true
  depends_on = [aws_internet_gateway.default]
}

resource "aws_nat_gateway" "default" {
  allocation_id = aws_eip.default.id
  subnet_id     = element(aws_subnet.public.*.id, 0)

  depends_on = [aws_internet_gateway.default]

  tags = {
    Name = "nat"
  }
}

resource "aws_subnet" "private" {
  count             = local.subnet_count
  vpc_id            = aws_vpc.default.id
  cidr_block = cidrsubnet(var.cidr_block, 4, count.index * 2) 
}

resource "aws_route_table" "private" {
  count  = local.subnet_count
  vpc_id = aws_vpc.default.id
}

resource "aws_route_table_association" "private" {
  count          = local.subnet_count
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

resource "aws_network_acl" "private" {
  vpc_id     = aws_vpc.default.id
  subnet_ids = aws_subnet.private.*.id

  egress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }

  ingress {
    rule_no    = 101
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }
}


resource "aws_subnet" "public" {
  count             = local.subnet_count
  vpc_id            = aws_vpc.default.id
  cidr_block = cidrsubnet(
    signum(length(var.cidr_block) == 1 ? var.cidr_block : aws_vpc.default.cidr_block),
    ceil(log(local.subnet_count * 2, 2)),
    count.index
  )
}

resource "aws_route_table" "public" {
  count  = local.subnet_count
  vpc_id = aws_vpc.default.id
}

resource "aws_route_table_association" "public" {
  count          = local.subnet_count
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}

resource "aws_route" "public_igw" {
  count          = local.subnet_count
  route_table_id = element(aws_route_table.public.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.default.id
}

resource "aws_route" "private_nat" {
  count          = local.subnet_count
  route_table_id = element(aws_route_table.private.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_nat_gateway.default.id
}

### Security group for public subnets

resource "aws_network_acl" "public" {
  vpc_id     = aws_vpc.default.id
  subnet_ids = aws_subnet.public.*.id


  ingress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 22
    to_port    = 22
    protocol   = "tcp"
  }

  ingress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 80
    to_port    = 80
    protocol   = "tcp"
  }

    ingress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 8080
    to_port    = 8080
    protocol   = "tcp"
  }

    ingress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 443
    to_port    = 443
    protocol   = "tcp"
  }
}
